//
//  ClearScoreTests.swift
//  ClearScoreTests
//
//  Created by Singh, Manjinder on 19/08/2021.
//

import XCTest
@testable import ClearScore

class ClearScoreTests: XCTestCase {

    let session = MockURLSession()
    var networkManager: NetworkManager?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        continueAfterFailure = false
        networkManager = NetworkManager(session)
    }

    func testDataIsReturned(){
        
        session.nextData = expectedScoreData
        networkManager?.getData(callback: { (result) in
            switch result {
            case .success(let data):
                do {
                    let _ = try JSONDecoder().decode(CreditReport.self, from: data)
                    XCTAssertTrue(true)
                } catch {
                   XCTFail()
                }
            case .failure(error: let errorString):
               XCTFail("\(errorString)")
            }
        })
    }
    
    
    func testResumeCalled(){
        let dataTask  = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        networkManager?.getData(callback: { (result) in
            
        })
        
        XCTAssert(dataTask.resumeWasCalled)
    }
    
    func testCSCircularViewModel(){
        do{
            let data = try JSONDecoder().decode(CreditReport.self, from: expectedScoreData!)
            if let info = data.creditReportInfo {
                let vm = CSCircularViewModel()
                vm.loadData(report: info)
                XCTAssertFalse(vm.maxScoreText.value == "out of 500")
                XCTAssertFalse(vm.scoreText.value == "5140")
                XCTAssertTrue(vm.scoreText.value == "620")
                XCTAssertTrue(vm.maxScoreText.value == "out of 700")
            } else {
                XCTFail("Post Json is not parsed")
            }
          
            
        }catch{
            XCTFail("Post Json is not parsed")
        }
    }
    
    func testDataManager() {
        session.nextData = expectedScoreData
        guard let nm = networkManager  else {
            XCTFail("No network Manager")
            return
        }
        let dataManager = DataManager(nm)
        dataManager.getData { (report, errorString) in
            if let error = errorString {
                XCTFail(error)
                return
            }
            
            XCTAssertNotNil(report)
        }
    }
}
