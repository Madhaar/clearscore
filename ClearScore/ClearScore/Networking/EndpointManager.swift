//
//  EndpointManager.swift
//  CScore
//
//  Created by Singh, Manjinder on 18/08/2021.
//

import Foundation

public typealias HTTPHeaders = [String:String]

public enum HTTPTask {
    case request
    
    // case requestParameters, requestParametersAndHeaders, download, upload...etc
    //Can extend if given more time and if requirement changes
}


public enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}

enum NetworkEnvironment {
    case qa
    case production
    case staging
}

struct  CreditAPI: EndPointType {
    // Just a way to show you can have different enviorment set here.
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .production: return "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/prod"
        case .qa: return "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/prod" // can be different.
        case .staging: return "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/prod" // can be different.
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
       
       return "/mockcredit/values"
          
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        return .request
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}
