//
//  NetworkManager.swift
//  CScore
//
//  Created by Singh, Manjinder on 17/08/2021.
//

import Foundation

protocol NetworkProtocol{
   
    typealias handler = (Result<Data>) -> Void
    
    func getData(callback: @escaping handler)
}

struct NetworkManager: NetworkProtocol {
   
    static let environment : NetworkEnvironment = .production
    
    var router: Router<CreditAPI>
    
    init(_ session: URLSessionProtocol) {
        router = Router<CreditAPI>(currentSession: session)
    }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<Any> {
        switch response.statusCode {
        case 200...299: return .success(true)
        case 401...500: return .failure(error: NetworkResponse.authenticationError.rawValue)
        case 501...599: return .failure(error: NetworkResponse.badRequest.rawValue)
        case 600: return .failure(error: NetworkResponse.outdated.rawValue)
        default: return .failure(error: NetworkResponse.failed.rawValue)
        }
    }
    
    func getData(callback: @escaping handler) {
    
        router.request(CreditAPI()) { (data, response, error) in
            
            if let err = error {
                callback(.failure(error: err.localizedDescription))
                return
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    if let value = data {
                        callback(.success(value))
                    }
                case .failure(let networkFailureError):
                    callback(.failure(error: networkFailureError.description))
                }
            }
        }
    }
}

public enum Result<Data> {
  case success(Data)
  case failure(error: String)
}

public enum NetworkResponse: String {
    case success
    case authenticationError = "Please authenticate first"
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "No Data."
    case unableToDecode = "Decode failed."
}
