//
//  DataManager.swift
//  CScore
//
//  Created by Singh, Manjinder on 18/08/2021.
//

import Foundation

// Data manager is used because in future if you want to store network data first (say in core data) then you could extend this class

typealias Handler = (CreditReport?, String?) -> Void

protocol DataManagerProtocol{
   
    func getData(completionHanlder: @escaping Handler)
}

class DataManager<NP: NetworkProtocol>: DataManagerProtocol {
    
   fileprivate let networkManager  : NP
    
    init(_ networkManager: NP) {
        self.networkManager = networkManager
    }
}

//Post
extension DataManager {
    
    func getData(completionHanlder: @escaping Handler) {
        self.networkManager.getData { (result) in
            switch result {
            case .success(let data):
                do {
                    let creditReport = try JSONDecoder().decode(CreditReport.self, from: data)
                    completionHanlder(creditReport, nil)
                } catch {
                    completionHanlder(nil, NetworkResponse.unableToDecode.rawValue)
                }
            case .failure(error: let errorString):
                completionHanlder(nil, errorString)
            }
        }
    }
}

