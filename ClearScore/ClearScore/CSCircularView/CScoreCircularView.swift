//
//  CircularView.swift
//  CScore
//
//  Created by Singh, Manjinder on 17/08/2021.
//

import UIKit

class CScoreCircularView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var maxScoreLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    var vm = CSCircularViewModel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInitialiser()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInitialiser()
    }
    
    func loadNib() -> UIView? {
        return Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?.first as? UIView
    }
    
    func commonInitialiser() {
        self.contentView = loadNib()
        if let contentView = self.contentView {
            self.addSubview(contentView)
            contentView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                contentView.topAnchor.constraint(equalTo: self.topAnchor),
                contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            ])
        }
        
        vm.scoreText.bind {[weak self] (text) in
            DispatchQueue.main.async {
                self?.scoreLabel.text = text
            }
        }
        
        vm.maxScoreText.bind {[weak self] (text) in
            DispatchQueue.main.async {
                self?.maxScoreLabel.text = text
            }
        }
    }
    
    override  func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 0.5 * bounds.size.width
        layer.borderWidth = 2
        layer.borderColor = UIColor.black.cgColor
    }
}
