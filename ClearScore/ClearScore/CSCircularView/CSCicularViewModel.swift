//
//  CSCicularViewModel.swift
//  ClearScore
//
//  Created by Singh, Manjinder on 20/08/2021.
//

import Foundation

struct CSCircularViewModel {
    
    var scoreText = Box("Loading")
    var maxScoreText = Box("out of ...")
    
    init() {
        
    }
    
    func loadData(report: CreditReportInfo) {
        if let score = report.score {
            scoreText.value = "\(String(describing: score))"
        }
       
        if let score = report.maxScoreValue {
            maxScoreText.value = "out of \(String(describing: score))"
        }
    }
}
