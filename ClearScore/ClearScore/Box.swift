//
//  Box.swift
//  ClearScore
//
//  Created by Singh, Manjinder on 20/08/2021.
//

import Foundation

public final class Box<T> {

  typealias Listener = (T) -> Void
  var listener: Listener?

  public var value: T {
    didSet {
      listener?(value)
    }
  }

  init(_ value: T) {
    self.value = value
  }

  func bind(listener: Listener?) {
    self.listener = listener
    listener?(value)
  }
}

public final class Box2<T> {

  typealias Listener = (T?) -> Void
  var listener: Listener?

  public var value: T? {
    didSet {
      listener?(value)
    }
  }

  init(_ value: T?) {
    self.value = value
  }

  func bind(listener: Listener?) {
    self.listener = listener
    listener?(value)
  }
}

