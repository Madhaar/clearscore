//
//  CreditScore.swift
//  CScore
//
//  Created by Singh, Manjinder on 18/08/2021.
//

import Foundation

public class CreditReport: Codable, ObservableObject {
    
    public var accountIDVStatus: String?
    @Published  public var creditReportInfo: CreditReportInfo?
    public var dashboardStatus: String?
    public var personaType: String?
    public var coachingSummary: CoachingSummary?
    public var augmentedCreditScore: Int?
    
    
    enum PersonalKeys: String, CodingKey {
        case accountIDVStatus   = "accountIDVStatus"
        case creditReportInfo = "creditReportInfo"
        case dashboardStatus = "dashboardStatus"
        case personaType = "personaType"
        case coachingSummary = "coachingSummary"
        case augmentedCreditScore = "augmentedCreditScore"
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: PersonalKeys.self)
        accountIDVStatus = try values.decodeIfPresent(String.self, forKey: .accountIDVStatus)
        creditReportInfo = try values.decodeIfPresent(CreditReportInfo.self, forKey: .creditReportInfo)
        dashboardStatus = try values.decodeIfPresent(String.self, forKey: .dashboardStatus)
        personaType = try values.decodeIfPresent(String.self, forKey: .personaType)
        coachingSummary = try values.decodeIfPresent(CoachingSummary.self, forKey: .coachingSummary)
        augmentedCreditScore = try values.decodeIfPresent(Int.self, forKey: .augmentedCreditScore)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: PersonalKeys.self)
        try container.encode(accountIDVStatus, forKey: .accountIDVStatus)
        try container.encode(creditReportInfo, forKey: .creditReportInfo)
        try container.encode(dashboardStatus, forKey: .dashboardStatus)
        try container.encode(personaType, forKey: .personaType)
        try container.encode(coachingSummary, forKey: .coachingSummary)
        try container.encode(augmentedCreditScore, forKey: .augmentedCreditScore)
    }
}

public class CreditReportInfo: Codable {
    public var score: Int?
    public var scoreBand: Int?
    public var clientRef: String?
    public var status: String?
    public var maxScoreValue: Int?
    public var minScoreValue: Int?
    
    // can use other fields if required
    
    enum PersonalKeys: String, CodingKey {
        case score = "score"
        case scoreBand = "scoreBand"
        case clientRef = "clientRef"
        case status = "status"
        case maxScoreValue = "maxScoreValue"
        case minScoreValue = "minScoreValue"
        // can add more cases if required
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: PersonalKeys.self)
        score = try values.decodeIfPresent(Int.self, forKey: .score)
        scoreBand = try values.decodeIfPresent(Int.self, forKey: .scoreBand)
        clientRef = try values.decodeIfPresent(String.self, forKey: .clientRef)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        maxScoreValue = try values.decodeIfPresent(Int.self, forKey: .maxScoreValue)
        minScoreValue = try values.decodeIfPresent(Int.self, forKey: .minScoreValue)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: PersonalKeys.self)
        try container.encode(score, forKey: .score)
        try container.encode(scoreBand, forKey: .scoreBand)
        try container.encode(clientRef, forKey: .clientRef)
        try container.encode(status, forKey: .status)
        try container.encode(maxScoreValue, forKey: .maxScoreValue)
        try container.encode(minScoreValue, forKey: .minScoreValue)
    }
}

public class CoachingSummary: Codable {
    // We don't need this for demo
}
