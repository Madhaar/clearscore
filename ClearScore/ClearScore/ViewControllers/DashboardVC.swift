//
//  ViewController.swift
//  CScore
//
//  Created by Singh, Manjinder on 17/08/2021.
//

import UIKit

class DashboardVC: UIViewController {

    @IBOutlet weak var scoreView: CScoreCircularView!
    fileprivate var viewModel: DashboardViewModel
    
    init(dashboardVM: DashboardViewModel) {
        self.viewModel = dashboardVM
        super.init(nibName: "DashboardVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.getData { [weak self] report, errorString in
            guard let report = report, let reportInfo = report.creditReportInfo else {
                // Show Error Alert
                return
            }
            
            self?.scoreView.vm.loadData(report: reportInfo)
        }
    }
}

