//
//  DashboardViewModel.swift
//  CScore
//
//  Created by Singh, Manjinder on 17/08/2021.
//

import Foundation

class DashboardViewModel {
    
   fileprivate var dataManager: DataManagerProtocol
    
    init(withDataManager: DataManagerProtocol) {
         dataManager = withDataManager
    }
    
    func getData(completionHandler: @escaping Handler) {
        self.dataManager.getData { (report, errorString) in
            completionHandler(report, errorString)
        }
    }
}
