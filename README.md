# README #


### What is this repository for? ###

This is for ClearScore test.

### How do I get set up? ###

Build with Xcode 12.4

### Improvement Scope ###

1. Better UI could be build 
2. Circular progress bar could be added.
2. Combine could be used instead of using completion blocks but i am still learning it to use 100 effectively. 
3. UI test could be used to test UI. 

### Other considerations

 Data manager is used so in future if you want to store network data first (say in core data) then you could extend this class
 